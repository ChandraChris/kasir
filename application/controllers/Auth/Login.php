<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){ //Load Model or Library here
		parent::__construct();
		$this->load->model('M_login');
	}

	public function index($user = '')
	{
		if ($this->session->userdata('masuk') == TRUE) {	
			if ($this->session->userdata('akses') == '1' || $this->session->userdata('akses') == '2') {
				redirect(base_url('Landing/Home/index/petugas'),'refresh');
			} else {
				redirect(base_url('Landing/Home/index/user'),'refresh');
			}
		}
		else {
			if ($user == '') 
			{
				$this->load->view('auth/login-petugas');
			} 
			elseif ($user == 'user') 
			{
				$this->load->view('auth/login-user');
			}
		}
	}

	public function auth($user)
	{
		if ($user == 'petugas') 
		{
			$email = htmlspecialchars($this->input->post('email', TRUE), ENT_QUOTES);
			$password = htmlspecialchars($this->input->post('password', TRUE), ENT_QUOTES);
			$cek_petugas = $this->M_login->auth_petugas($email, $password, $user);
			if ($cek_petugas->num_rows() > 0) {
				$data = $cek_petugas->row_array();
				$this->session->set_userdata('masuk', TRUE);
				if ($data['level'] == '1') {
					$this->session->set_userdata('ses_id', $data['nip']);
					$this->session->set_userdata('ses_email', $data['email']);
					$this->session->set_userdata('ses_nama', $data['name']);
					$this->session->set_userdata('ses_gender', $data['gender']);
					$this->session->set_userdata('akses', '1');
					redirect(base_url('Landing/Home/index/petugas'),'refresh');
				} elseif ($data['level'] == '2') {
					$this->session->set_userdata('ses_id', $data['nip']);
					$this->session->set_userdata('ses_email', $data['email']);
					$this->session->set_userdata('ses_nama', $data['name']);
					$this->session->set_userdata('ses_gender', $data['gender']);
					$this->session->set_userdata('akses', '2');
					redirect(base_url('Landing/Home/index/petugas'),'refresh');
				}
			}
			else {
				redirect(base_url('Auth/Login/'),'refresh');
			}
		}
		elseif ($user == 'user') 
		{
			$email = htmlspecialchars($this->input->post('email', TRUE), ENT_QUOTES);
			$password = htmlspecialchars($this->input->post('password', TRUE), ENT_QUOTES);
			$cek_user = $this->M_login->auth_petugas($email, $password, $user);
			if ($cek_user->num_rows() > 0) {
				$data = $cek_user->row_array();
				$this->session->set_userdata('masuk', TRUE);
				$this->session->set_userdata('ses_id', $data['niu']);
				$this->session->set_userdata('ses_email', $data['email']);
				$this->session->set_userdata('ses_nama', $data['name']);
				$this->session->set_userdata('ses_gender', $data['gender']);
				$this->session->set_userdata('akses', '3');
				redirect(base_url('Landing/Home/index/user'),'refresh');
			} else {
				redirect(base_url('Auth/Login/'),'refresh');
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url(),'refresh');
	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Auth/Login.php */