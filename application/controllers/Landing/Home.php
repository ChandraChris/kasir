<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_crud');
		$this->load->model('M_login');
		if ($this->session->userdata('masuk') != TRUE) {
			redirect(base_url('Auth/Login'),'refresh');
		}
	}
	public function index($user)
	{
		if ($this->session->userdata('akses') == '1' || $this->session->userdata('akses') == '2') {	
			if ($user == 'petugas') 
			{
				$this->template->load('petugas/index', 'petugas/profile');
			// echo "<pre>";
			// print_r($this->session->userdata);
			// echo "</pre>";
			}
			elseif ($user == 'user') 
			{
				redirect(base_url('Landing/Home/index/petugas'),'refresh');
			// echo "<pre>";
			// print_r($this->session->userdata);
			// echo "</pre>";
			} 
		} else {
			if ($user == 'petugas') 
			{
			// echo "<pre>";
			// print_r($this->session->userdata);
			// echo "</pre>";
				redirect(base_url('Landing/Home/index/user'),'refresh');	
			}
			elseif ($user == 'user') 
			{
				$this->template->load('user/index', 'user/profile');
			// echo "<pre>";
			// print_r($this->session->userdata);
			// echo "</pre>";
			}
		}
	}
	public function update_profile($user)
	{
		if ($user == 'petugas') {	
			$nip = $this->input->post('nip');
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$gender = $this->input->post('gender');

			$data = array(
				'name' => $name,
				'email' => $email,
				'gender' => $gender
			);

			$where = array('nip' => $nip);
			$update = $this->M_crud->update_profile($where, $data, $user);
			if ($update > 0) 
			{	
				$cek_petugas = $this->M_login->update_petugas_auth($email, $nip, $user);
				$data = $cek_petugas->row_array();
				$this->session->set_userdata('ses_nama', $data['name']);
				$this->session->set_userdata('ses_email', $data['email']);
				$this->session->set_userdata('ses_gender', $data['gender']);
				$this->session->set_flashdata('relog', '<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Success Update Data!</strong> Please relog to Optimize Your Data.
					</div>');
				redirect(base_url('Landing/Home/index/' . $user),'refresh');
			}
			else 
			{
				$cek_petugas = $this->M_login->update_petugas_auth($email, $nip, $user);
				$data = $cek_petugas->row_array();
				$this->session->set_userdata('ses_nama', $data['name']);
				$this->session->set_userdata('ses_email', $data['email']);
				$this->session->set_userdata('ses_gender', $data['gender']);
				$this->session->set_flashdata('relog', '<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Failed Update Data!</strong> Please Contact admin for this problem.
					</div>');
				redirect(base_url('Landing/Home/index/' . $user),'refresh');	
			}
		}
		elseif ($user == 'user') {
			$niu = $this->input->post('niu');
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$gender = $this->input->post('gender');

			$data = array(
				'name' => $name,
				'email' => $email,
				'gender' => $gender
			);

			$where = array('niu' => $niu);
			$update = $this->M_crud->update_profile($where, $data, $user);
			if ($update > 0) 
			{	
				$cek_petugas = $this->M_login->update_petugas_auth2($email, $niu, $user);
				$data = $cek_petugas->row_array();
				$this->session->set_userdata('ses_nama', $data['name']);
				$this->session->set_userdata('ses_email', $data['email']);
				$this->session->set_userdata('ses_gender', $data['gender']);
				$this->session->set_flashdata('relog', '<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Success Update Data!</strong> Please relog to Optimize Your Data.
					</div>');
				redirect(base_url('Landing/Home/index/' . $user),'refresh');
			}
			else 
			{
				$cek_petugas = $this->M_login->update_petugas_auth($email, $nip, $user);
				$data = $cek_petugas->row_array();
				$this->session->set_userdata('ses_nama', $data['name']);
				$this->session->set_userdata('ses_email', $data['email']);
				$this->session->set_userdata('ses_gender', $data['gender']);
				$this->session->set_flashdata('relog', '<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Failed Update Data!</strong> Please Contact admin for this problem.
					</div>');
				redirect(base_url('Landing/Home/index/' . $user),'refresh');	
			}
		}
		
	}
	public function barang($user){
		$data['barang'] = $this->M_crud->read('barang')->result();
		if ($user == 'petugas') 
		{
			$this->template->load('petugas/index', 'petugas/barang', $data);
		}
		else 
		{
			$this->template->load('user/index', 'user/barang', $data);
		}
	}
	public function petugas()
	{
		$data['petugas'] = $this->M_crud->read('petugas')->result();
		$this->template->load('petugas/index', 'petugas/list/all', $data);
	}
	public function edit_petugas()
	{
		if ($this->session->userdata('akses') == '1') {	
			$data['petugas'] = $this->M_crud->read('petugas')->result();
			$this->template->load('petugas/index', 'petugas/list/edit', $data);
		} else {
			$this->template->load('petugas/index', 'welcome_message');
		}
	}
	public function transaksi($user)
	{
		if ($user == 'petugas') {
			$data['transaksi'] = $this->M_crud->read('transaksi')->result();
			$this->template->load('petugas/index', 'petugas/transaksi', $data);
		} elseif ($user == 'user') {
			$name = $this->session->userdata('ses_nama');
			$data['transaksi'] = $this->M_login->mytrans($name)->result();
			$this->template->load('user/index', 'user/transaksi', $data);
		}
	}
	public function get_transaksi($id)
	{
		$where = array('id_barang' => $id);

		$data['transaksi'] = $this->M_crud->data($where, 'barang')->result();
		$this->template->load('user/index', 'user/get-transaksi', $data);
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Landing/Home.php */