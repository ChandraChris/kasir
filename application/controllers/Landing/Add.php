<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('masuk') != true) {
			redirect(base_url(),'refresh');
		}
		$this->load->model('M_crud');
	}
	public function index($item)
	{
		if ($item == 'barang') 
		{
			$id = htmlspecialchars($this->input->post('id', TRUE), ENT_QUOTES);
			$barang = htmlspecialchars($this->input->post('barang', TRUE), ENT_QUOTES);
			$stok = htmlspecialchars($this->input->post('stok', TRUE), ENT_QUOTES);
			$harga = htmlspecialchars($this->input->post('harga', TRUE), ENT_QUOTES);
			$moderator = htmlspecialchars($this->input->post('moderator', TRUE), ENT_QUOTES);

			$data = array(
				'id_barang' => $id,
				'barang' => $barang,
				'stok' => $stok,
				'harga' => $harga,
				'moderator' => $moderator
			);
			$this->M_crud->add($data, 'barang');
			redirect(base_url('Landing/Home/barang/petugas'),'refresh');
		}
		elseif ($item == 'petugas') 
		{
			$nip = htmlspecialchars($this->input->post('nip', TRUE), ENT_QUOTES);
			$name = htmlspecialchars($this->input->post('name', TRUE), ENT_QUOTES);
			$gender = htmlspecialchars($this->input->post('gender', TRUE), ENT_QUOTES);
			$email = htmlspecialchars($this->input->post('email', TRUE), ENT_QUOTES);
			$level = htmlspecialchars($this->input->post('level', TRUE), ENT_QUOTES);

			$data = array(
				'nip' => $nip,
				'name' => $name,
				'gender' => $gender,
				'email' => $email,
				'level' => $level
			);
			$this->M_crud->add($data, 'petugas');
			redirect(base_url('Landing/Home/edit_petugas'),'refresh');
		}
	}

	public function delete($item, $id)
	{
		if ($item == 'barang') 
		{
			$where = array(
				'id_barang' => $id
			);
			$this->M_crud->delete($where, 'barang');
			redirect(base_url('Landing/Home/barang/petugas'),'refresh');
		}
		elseif ($item == 'petugas') 
		{
			$where = array('nip' => $id);
			$this->M_crud->delete($where, 'petugas');
			redirect(base_url('Landing/Home/edit_petugas'),'refresh');
		}
	}
	public function update($item, $id)
	{
		if ($item == 'barang') {
			$where = array(
				'id_barang' => $id
			);
			$data['barang'] = $this->M_crud->data($where, 'barang')->result();
			$this->template->load('petugas/index', 'petugas/form-barang/edit-barang', $data);
		} elseif ($item == 'petugas') {
			$where = array(
				'nip' => $id
			);
			$data['petugas'] = $this->M_crud->data($where, 'petugas')->result();
			$this->template->load('petugas/index', 'petugas/list/detail-edit', $data);
		}
	}
	public function process_update($item)
	{
		if ($item == 'barang') {
			$id = htmlspecialchars($this->input->post('id', TRUE), ENT_QUOTES);
			$barang = htmlspecialchars($this->input->post('barang', TRUE), ENT_QUOTES);
			$stok = htmlspecialchars($this->input->post('stok', TRUE), ENT_QUOTES);
			$harga = htmlspecialchars($this->input->post('harga', TRUE), ENT_QUOTES);
			$moderator = htmlspecialchars($this->input->post('moderator', TRUE), ENT_QUOTES);

			$data = array(
				'barang' => $barang,
				'stok' => $stok,
				'harga' => $harga,
				'moderator' => $moderator
			);
			$where = array('id_barang' => $id);
			$this->M_crud->update_profile($where, $data, 'barang');
			redirect(base_url('Landing/Home/barang/petugas'),'refresh');
		} elseif ($item == 'petugas') {
			$nip = htmlspecialchars($this->input->post('nip', TRUE), ENT_QUOTES);
			$name = htmlspecialchars($this->input->post('name', TRUE), ENT_QUOTES);
			$gender = htmlspecialchars($this->input->post('gender', TRUE), ENT_QUOTES);
			$email = htmlspecialchars($this->input->post('email', TRUE), ENT_QUOTES);
			$level = htmlspecialchars($this->input->post('level', TRUE), ENT_QUOTES);

			$data = array(
				'name' => $name,
				'gender' => $gender,
				'email' => $email,
				'level' => $level
			);
			$where = array('nip' => $nip);
			$this->M_crud->update_profile($where, $data, 'petugas');
			redirect(base_url('Landing/Home/edit_petugas'),'refresh');
		}
	}
	public function add_transaksi()
	{
		$id = htmlspecialchars($this->input->post('id', TRUE), ENT_QUOTES);
		$barang = htmlspecialchars($this->input->post('barang', TRUE), ENT_QUOTES);
		$stok = htmlspecialchars($this->input->post('stok', TRUE), ENT_QUOTES);
		$buyer = htmlspecialchars($this->input->post('buyer', TRUE), ENT_QUOTES);
		$harga = htmlspecialchars($this->input->post('harga', TRUE), ENT_QUOTES);
		$total = $stok * $harga;

		$data = array(
			'id_barang' => $id,
			'barang' => $barang,
			'stok' => $stok,
			'total_harga' => $total,
			'buyer' => $buyer
		);
		$this->M_crud->add($data, 'transaksi');
		redirect(base_url('Landing/Home/barang/user'),'refresh');
	}
}

/* End of file Add.php */
/* Location: ./application/controllers/Landing/Add.php */