<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_crud extends CI_Model {

	function read($table)
	{
		$read = $this->db->get($table);
		return $read;
	}
	function add($data, $table)
	{
		$add = $this->db->insert($table, $data);
		return $add;
	}
	function update_profile($where, $data, $table)
	{
		$this->db->where($where);
		$update = $this->db->update($table, $data);
		return $update;
	}
	function data($where, $table)
	{
		return $this->db->get_where($table, $where);
	}
	function delete($where, $table)
	{
		$this->db->where($where);
		$delete = $this->db->delete($table);
		return $delete;
	}
}

/* End of file M_crud.php */
/* Location: ./application/models/M_crud.php */