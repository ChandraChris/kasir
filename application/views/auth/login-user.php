<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<?php $this->load->view('partials/head'); ?>
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/form-login.css') ?>">
</head>
<body id="LoginForm">
	<div class="container">
		<h1 class="form-heading">Login Form</h1>
		<div class="login-form">
			<div class="main-div">
				<div class="panel">
					<h2>Login User</h2>
					<p>Please enter your email and password</p>
				</div>
				<form id="Login" action="<?= base_url('Auth/Login/auth/user') ?>" method="POST" autocomplete="off">
					<div class="form-group">
						<input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email Address" required>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password"
						required>
					</div>
					<div class="forgot">
						<a href="<?= base_url('Auth/Login') ?>">Not User?</a>
					</div>
					<button type="submit" class="btn btn-primary">Login</button>
				</form>
			</div>
		</div>
	</div>
	<?php $this->load->view('partials/script'); ?>
</body>
</html>