<!DOCTYPE html>
<html>
<head>
	<title>Home-User</title>
	<?php $this->load->view('partials/head'); ?>
</head>
<body>
	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<p class="navbar-brand" href="">Hello <?php echo $this->session->userdata('ses_nama');?></p>
			</div>
			<!-- /.navbar-header -->

			<ul class="nav navbar-top-links navbar-right">
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
	                    <li><a href="<?php echo base_url('Auth/Login/logout')?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
	                    </li>
	                </ul>
	                <!-- /.dropdown-user -->
	            </li>
	            <!-- /.dropdown -->
	        </ul>
	        <!-- /.navbar-top-links -->

	        <div class="navbar-default sidebar" role="navigation">
	        	<div class="sidebar-nav navbar-collapse">
	        		<ul class="nav" id="side-menu">
	        			<li>
	        				<a href="<?php echo base_url('Landing/Home/index/petugas') ?>"><i class="fa fa-home fa-fw"></i> Home</a>
	        			</li>
	        			<li>
	        				<a href="<?php echo base_url('Landing/Home/barang/user') ?>"><i class="fa fa-table fa-fw"></i> Barang</a>
	        			</li>
	        			<li>
	        				<a href="<?php echo base_url('Landing/Home/transaksi/user') ?>"><i class="fa fa-shopping-cart fa-fw"></i> My Transaksi</a>
	        			</li>
	        		</ul>
	        	</div>
	        	<!-- /.sidebar-collapse -->
	        </div>
	        <!-- /.navbar-static-side -->
	    </nav>
	    <!-- /#page-wrapper -->
	    <?php echo $contents ?>
	</div>
	<?php $this->load->view('partials/script'); ?>
</body>
</html>