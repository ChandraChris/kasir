<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Transaksi</h1>
			<?php foreach ($transaksi as $row): ?>	
				<form autocomplete="off" class="form-horizontal" action="<?= base_url('Landing/Add/add_transaksi');?>"
					method = "POST">
					<div class="form-group">
						<label class="control-label col-sm-2" for="id">id : </label>
						<div class="col-sm-10">
							<input readonly type="text" class="form-control" id="id" placeholder="Enter id"
							name="id" value="<?= $row->id_barang ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="barang">Barang : </label>
						<div class="col-sm-10">
							<input readonly type="text" class="form-control" id="barang" placeholder="Enter barang" required
							name="barang" value="<?= $row->barang ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="stok">Pcs : </label>
						<div class="col-sm-10">
							<input type="number" class="form-control" id="stok" placeholder="Enter pcs" required
							name="stok"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="harga">Harga : </label>
						<div class="col-sm-10">
							<input readonly type="number" class="form-control" id="harga" placeholder="Price" required
							name="harga" value="<?= $row->harga ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="buyer">Buyer : </label>
						<div class="col-sm-10">
							<input readonly type="text" class="form-control" id="buyer" placeholder="Enter buyer" required
							name="buyer" value="<?= $this->session->userdata('ses_nama')?>" />
						</div>
					</div>
					<center>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-warning"><i class="fa fa-plus-square fa-fw"></i> Transaction</button>
							</div>
						</div>
					</center>
				</form>
			<?php endforeach ?>
		</div>
	</div>
</div>