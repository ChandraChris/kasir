<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Transaksi</h1>
			<div class="table-responsive">
				<table id="dataTable" class="table table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Barang</th>
							<th>Stok</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($transaksi as $row): ?>
							<tr>
								<td><?php echo $row->id_barang ?></td>
								<td><?php echo $row->barang ?></td>
								<td><?php echo $row->stok ?></td>
								<td>Rp. <?php echo $row->total_harga ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>