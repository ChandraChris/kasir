<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Barang</h1>
			<div class="table-responsive">
				<table id="dataTable" class="table table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Barang</th>
							<th>Stok</th>
							<th>Harga</th>
							<th>Moderator</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($barang as $row): ?>
							<tr>
								<td><?php echo $row->id_barang ?></td>
								<td><?php echo $row->barang ?></td>
								<td><?php echo $row->stok ?></td>
								<td>Rp. <?php echo $row->harga ?></td>
								<td><?php echo $row->moderator ?></td>
								<td>
									<a href="<?= base_url('Landing/Home/get_transaksi/' . $row->id_barang) ?>" class="btn btn-warning">Transaksi</a>
									</a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>