<!-- Script Jquery -->
<script src="<?= base_url('assets/js/jquery-3.3.1.js') ?>" type="text/javascript"></script>
<!-- Script Bootstrap -->
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>" type="text/javascript"></script>
<!-- Metis Menu -->
<script src="<?php echo base_url('assets/dashboard-admin/vendor/metisMenu/metisMenu.min.js');?>"></script>
<!-- Morris Charts -->
<script src="<?php echo base_url('assets/dashboard-admin/vendor/raphael/raphael.min.js');?>"></script>
<script src="<?php echo base_url('assets/dashboard-admin/vendor/morrisjs/morris.min.js');?>"></script>
<script src="<?php echo base_url('assets/dashboard-admin/data/morris-data.js');?>"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url('assets/dashboard-admin/dist/js/sb-admin-2.js');?>"></script>
<script src="<?= base_url('assets/DataTables/datatables.min.js') ?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#dataTable').DataTable();
	});
</script>