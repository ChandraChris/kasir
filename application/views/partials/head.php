<!-- Link to Bootstrap -->
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
<!-- Link MetisMenu -->
<link href="<?php echo base_url('assets/dashboard-admin/vendor/metisMenu/metisMenu.min.css');?>" rel="stylesheet">
<!-- Link Dashboard -->
<link href="<?php echo base_url('assets/dashboard-admin/dist/css/sb-admin-2.css');?>" rel="stylesheet">
<!-- Morris Charts CSS -->
<link href="<?php echo base_url('assets/dashboard-admin/vendor/morrisjs/morris.css');?>" rel="stylesheet">
<!-- Custom Fonts -->
<link href="<?php echo base_url('assets/dashboard-admin/vendor/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
<!-- Link to DataTables -->
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/DataTables/datatables.min.css') ?>">