<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Profile</h1>
			<center>
				<?php if ($this->session->userdata('ses_gender') == 'men') 
				{
					echo "<img class = 'img-fluid'" . 'src=' . "'" . base_url('assets/img/icon-men.png') . "' " . "height = '300px'>";
				}
				else 
				{
					echo "<img class = 'img-fluid'" . 'src=' . "'" . base_url('assets/img/icon-women.png') . "' " . "height = '300px'>";
				}
				?>	
			</center>
			<h5>ID : <?php echo $this->session->userdata('ses_id'); ?></h5>
			<h5>Name : <?php echo $this->session->userdata('ses_nama'); ?></h5>
			<h5>Email : <?php echo $this->session->userdata('ses_email') ?></h5>
			<h5>Gender : <?php echo $this->session->userdata('ses_gender') ?></h5>
			<?php echo $this->session->flashdata('relog'); ?>
			<br />
			<form class="form-horizontal" action="<?= base_url('Landing/Home/update_profile/petugas');?>" method="POST" autocomplete="off">
				<div class="form-group">
					<label class="control-label col-sm-2" for="nip">ID : </label>
					<div class="col-sm-10">
						<input readonly type="text" name="nip" class="form-control" id="nip" value="<?php echo $this->session->userdata('ses_id') ?>">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="name">Name : </label>
					<div class="col-sm-10">
						<input type="text" name="name" class="form-control" id="name" value="<?php echo $this->session->userdata('ses_nama') ?>">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="email">Email : </label>
					<div class="col-sm-10">
						<input type="email" name="email" id="email" class="form-control" value="<?php echo $this->session->userdata('ses_email') ?>">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="sel1">Select Gender:</label>
					<div class="col-sm-10">	
						<select class="form-control" id="sel1" name="gender">
							<option value="men">Men</option>
							<option value="women">Women</option>
						</select>
					</div>
				</div>
				<center>
					<button class="btn btn-success">Update</button>
				</center>
				<br />
			</form>
		</div>
	</div>
</div>