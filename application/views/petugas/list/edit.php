<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Edit Petugas</h1>
			<div class="table-responsive">
				<table id="dataTable" class="table table-hover">
					<thead>
						<tr>
							<th>NIP</th>
							<th>Name</th>
							<th>Gender</th>
							<th>Email</th>
							<th>Level</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($petugas as $row): ?>
							<tr>
								<td><?php echo $row->nip ?></td>
								<td><?php echo $row->name ?></td>
								<td><?php echo $row->gender ?></td>
								<td><?php echo $row->email ?></td>
								<td><?php echo $row->level ?></td>
								<td>
									<a href="<?= base_url('Landing/Add/update/petugas/' . $row->nip) ?>" class="btn btn-info">Edit</a> || <a href="<?= base_url('Landing/Add/delete/petugas/' . $row->nip) ?>" class="btn btn-danger">Delete
									</a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				<form class="form-horizontal" action="<?= base_url('Landing/Add/index/petugas');?>" method="POST" autocomplete="off">
					<div class="form-group">
						<label class="control-label col-sm-2" for="nip">NIP : </label>
						<div class="col-sm-10">
							<input type="number" name="nip" class="form-control" id="nip">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="name">Name : </label>
						<div class="col-sm-10">
							<input type="text" name="name" class="form-control" id="name">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="selgen">Gender:</label>
						<div class="col-sm-10">	
							<select class="form-control" id="selgen" name="gender">
								<option value="men">Men</option>
								<option value="women">Women</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Email : </label>
						<div class="col-sm-10">
							<input type="email" name="email" id="email" class="form-control" >
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="level">Level : </label>
						<div class="col-sm-10">	
							<input type="number" name="level" id="level" class="form-control">
						</div>
					</div>
					<center>
						<button class="btn btn-success">Add</button>
					</center>
					<br />
				</form>
			</div>
		</div>
	</div>
</div>