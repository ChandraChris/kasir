<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">All Petugas</h1>
			<div class="table-responsive">
				<table id="dataTable" class="table table-hover">
					<thead>
						<tr>
							<th>NIP</th>
							<th>Name</th>
							<th>Gender</th>
							<th>Email</th>
							<th>Level</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($petugas as $row): ?>
							<tr>
								<td><?php echo $row->nip ?></td>
								<td><?php echo $row->name ?></td>
								<td><?php echo $row->gender ?></td>
								<td><?php echo $row->email ?></td>
								<td><?php echo $row->level ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>