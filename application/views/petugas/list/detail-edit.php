<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Edit Petugas</h1>
			<?php foreach ($petugas as $row): ?>	
				<form autocomplete="off" class="form-horizontal" action="<?= base_url('Landing/Add/process_update/petugas');?>"
					method = "POST">
					<div class="form-group">
						<label class="control-label col-sm-2" for="nip">NIP : </label>
						<div class="col-sm-10">
							<input readonly type="text" class="form-control" id="nip" placeholder="Enter nip"
							name="nip" value="<?= $row->nip ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="name">Name : </label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="name" placeholder="Enter name" required
							name="name" value="<?= $row->name ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="selgen">Gender:</label>
						<div class="col-sm-10">	
							<select class="form-control" id="selgen" name="gender">
								<option value="men">Men</option>
								<option value="women">Women</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Email : </label>
						<div class="col-sm-10">
							<input type="email" class="form-control" id="email" placeholder="Enter email" required
							name="email" value="<?= $row->email ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="level">Level : </label>
						<div class="col-sm-10">
							<input type="number" class="form-control" id="level" placeholder="Enter level" required
							name="level" value="<?= $row->level ?>" />
						</div>
					</div>
					<center>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-info"><i class="fa fa-plus-square fa-fw"></i> Edit</button>
							</div>
						</div>
					</center>
				</form>
			<?php endforeach ?>
		</div>
	</div>
</div>