<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Barang</h1>
			<div class="table-responsive">
				<table id="dataTable" class="table table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Barang</th>
							<th>Stok</th>
							<th>Harga</th>
							<th>Moderator</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($barang as $row): ?>
							<tr>
								<td><?php echo $row->id_barang ?></td>
								<td><?php echo $row->barang ?></td>
								<td><?php echo $row->stok ?></td>
								<td>Rp. <?php echo $row->harga ?></td>
								<td><?php echo $row->moderator ?></td>
								<td>
									<a href="<?= base_url('Landing/Add/update/barang/' . $row->id_barang) ?>" class="btn btn-info">Edit</a> || <a href="<?= base_url('Landing/Add/delete/barang/' . $row->id_barang) ?>" class="btn btn-danger">Delete
									</a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				<form class="form-horizontal" action="<?= base_url('Landing/Add/index/barang');?>" method="POST" autocomplete="off">
					<div class="form-group">
						<label class="control-label col-sm-2" for="id">ID : </label>
						<div class="col-sm-10">
							<input type="text" name="id" class="form-control" id="id">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="barang">Barang : </label>
						<div class="col-sm-10">
							<input type="text" name="barang" class="form-control" id="barang">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="stok">Stok : </label>
						<div class="col-sm-10">
							<input type="number" name="stok" id="stok" class="form-control" >
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="harga">Harga : </label>
						<div class="col-sm-10">	
							<input type="number" name="harga" id="harga" class="form-control" placeholder="Tidak Usah Pakai RP">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10">	
							<input type="hidden" name="moderator" class="form-control" value="<?php echo $this->session->userdata('ses_nama');?>">
						</div>
					</div>
					<center>
						<button class="btn btn-success">Add</button>
					</center>
					<br />
				</form>
			</div>
		</div>
	</div>
</div>