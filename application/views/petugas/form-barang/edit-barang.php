<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Edit Barang</h1>
			<?php foreach ($barang as $row): ?>	
				<form autocomplete="off" class="form-horizontal" action="<?= base_url('Landing/Add/process_update/barang');?>"
					method = "POST">
					<div class="form-group">
						<label class="control-label col-sm-2" for="id">ID : </label>
						<div class="col-sm-10">
							<input readonly type="text" class="form-control" id="id" placeholder="Enter id"
							name="id" value="<?= $row->id_barang ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="barang">Barang : </label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="barang" placeholder="Enter barang" required
							name="barang" value="<?= $row->barang ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="stok">stok : </label>
						<div class="col-sm-10">
							<input type="number" class="form-control" id="stok" placeholder="Enter stok" required
							name="stok" value="<?= $row->stok ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="harga">harga : </label>
						<div class="col-sm-10">
							<input type="number" class="form-control" id="harga" placeholder="Enter harga" required
							name="harga" value="<?= $row->harga ?>" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10">
							<input type="hidden" class="form-control" required
							name="moderator" value="<?= $this->session->userdata('ses_nama'); ?>" />
						</div>
					</div>
					<center>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-info"><i class="fa fa-plus-square fa-fw"></i> Edit</button>
							</div>
						</div>
					</center>
				</form>
			<?php endforeach ?>
		</div>
	</div>
</div>