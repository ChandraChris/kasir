-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 24, 2018 at 02:37 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasir`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` varchar(11) NOT NULL,
  `barang` varchar(255) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `moderator` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `barang`, `stok`, `harga`, `moderator`) VALUES
('A1', 'Mouse Rexus', 23, 75000, 'Admin'),
('A2', 'Keyboard Razer', 7, 250000, 'Ferguso'),
('A3', 'Laptop', 3, 6000000, 'Admin'),
('A66', 'Earphone', 100, 230000, 'Ferguso'),
('B12', 'Android M', 10, 2000000, 'Admin'),
('C14', 'Coffee Maker', 91, 300000, 'Ferguso'),
('D76', 'Dompet Pria', 65, 125000, 'Admin'),
('F11', 'Kursi Gaming', 5, 1500000, 'Admin'),
('K45', 'Stop Kontak', 210, 34000, 'Admin'),
('M12', 'Kabel LAN', 1000, 10000, 'Ferguso'),
('P18', 'Printer Panasonic', 12, 1200000, 'Ferguso');

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `nip` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` varchar(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`nip`, `name`, `gender`, `email`, `password`, `level`) VALUES
(10001, 'Admin', 'men', 'admin@gmail.com', '70359abdfe7283015fa280fea6b7508d', 1),
(10002, 'Fergusa', 'men', 'ferguso@gmail.com', 'a6be5632d5dd4a9e31b3acda6b98497c', 2);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `id_barang` varchar(11) NOT NULL,
  `barang` varchar(255) NOT NULL,
  `stok` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `buyer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `id_barang`, `barang`, `stok`, `total_harga`, `buyer`) VALUES
(7, 'A2', 'Keyboard Razer', 5, 1250000, 'Dani'),
(8, 'A1', 'Mouse Rexus', 8, 600000, 'Dani'),
(9, 'A3', 'Laptop', 1, 6000000, 'Dikola');

--
-- Triggers `transaksi`
--
DELIMITER $$
CREATE TRIGGER `penjualan_barang` AFTER INSERT ON `transaksi` FOR EACH ROW BEGIN
	UPDATE barang SET stok = stok-NEW.stok
    WHERE id_barang = NEW.id_barang;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `niu` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`niu`, `name`, `gender`, `email`, `password`) VALUES
(10001, 'Dani', 'men', 'buyer@gmail.com', '6b048ddfa112791ffa1fc1b8ed3b5f90'),
(10002, 'Fergusa', '', 'fergusa@gmail.com', '7fde81f1338f6cebc9dfda9a437533ee'),
(10003, 'Dikola', 'women', 'dikola@gmail.com', '19329ceb237dcc7f34e11ce7edca3ad0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`niu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
